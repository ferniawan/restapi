import express from 'express';
import config from '../config';
import middleware from '../middleware';
import initializeDb from '../db';
import account from '../controller/account';
import province from '../controller/province';
import district from '../controller/district';
import subdistrict from '../controller/subdistrict';
import village from '../controller/village';
import poiclass from '../controller/poiclass';
import poisubclass from '../controller/poisubclass';
import poi from '../controller/poi';

let router = express();

//connect to db
initializeDb(db=>{

  //internal middleware
  router.use(middleware({config,db}));

  //api routes v1 (/v1)
  router.use('/account', account({config,db}));
  router.use('/province', province({config,db}));
  router.use('/district', district({config,db}));
  router.use('/subdistrict', subdistrict({config,db}));
  router.use('/village', village({config,db}));
  router.use('/poi', poi({config,db}));
  router.use('/poiclass', poiclass({config,db}));
  router.use('/poisubclass', poisubclass({config,db}));

});

export default router;
