import Sequelize from 'sequelize';
import initializeDb from '../db';

initializeDb(db=>{
  const Task = db.define('tb_area_vi',{
    su_name: Sequelize.STRING
   });
  module.exports = Task;
});
