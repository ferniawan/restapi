import Sequelize from 'sequelize';
import initializeDb from '../db';

initializeDb(db=>{
  const Task = db.define('tb_poiclass',{
    clname: Sequelize.STRING
   });
  module.exports = Task;
});
