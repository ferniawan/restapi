
import Sequelize from 'sequelize';
import passportLocalSequelize from 'passport-local-sequelize';
import initializeDb from '../db';

initializeDb(db=>{

  const Account = db.define('Account',
  {name: Sequelize.STRING,
	myhash: Sequelize.STRING,
	mysalt: Sequelize.STRING});
  passportLocalSequelize.attachToUser(Account,{
    usernameField: 'name',
    hashField: 'myhash',
    saltField: 'mysalt'
  });

  module.exports = Account;
});
