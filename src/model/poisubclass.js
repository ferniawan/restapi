import Sequelize from 'sequelize';
import initializeDb from '../db';

initializeDb(db=>{
  const Task = db.define('tb_poisubclass',{
    scname: Sequelize.STRING
   });
  module.exports = Task;
});
