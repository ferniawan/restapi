import Sequelize from 'sequelize';
import initializeDb from '../db';

initializeDb(db=>{
  const Task = db.define('tb_poi',{
    poi_id: Sequelize.STRING
   });
  module.exports = Task;
});
