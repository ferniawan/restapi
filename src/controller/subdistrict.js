import Sequelize from 'sequelize';
import { Router } from 'express';
import Subdistrict from '../model/subdistrict';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    Subdistrict.findAll({
      attributes: [[db.col('su_name'), 'area_name'], 'su_id' , [db.col('su_geom'), 'geom']]
    }).then(tb_area_su => {
      res.status(200).send(tb_area_su);
    })

  });

  // '/v1/village' - GET all village
  api.get('/:id', authenticate,(req, res) => {

    Subdistrict.findAll({
      attributes: [[db.col('su_name'), 'area_name'], 'su_id' , [db.col('su_geom'), 'geom']],
      where:{di_id:req.params.id }
    }).then(tb_area_su => {
      res.status(200).send(tb_area_su);
    })

  });
  return api;
}
