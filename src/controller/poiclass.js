import Sequelize from 'sequelize';
import { Router } from 'express';
import Poiclass from '../model/poiclass';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    Poiclass.findAll({
      attributes: ['clname', 'clid']
    }).then(tb_poiclass => {
      res.status(200).send(tb_poiclass);
    })

  });


    return api;
}
