import Sequelize from 'sequelize';
import { Router } from 'express';
import District from '../model/district';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/district' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    District.findAll({
      attributes: [[db.col('di_name'), 'area_name'], 'di_id' , [db.col('di_geom'), 'geom']]
    }).then(tb_area_di => {
      res.status(200).send(tb_area_di);
    })

  });

  // '/v1/district' - GET all village
  api.get('/:id', authenticate,(req, res) => {

    District.findAll({
      attributes: [[db.col('di_name'), 'area_name'], 'di_id' , [db.col('di_geom'), 'geom']],
      where:{pr_id:req.params.id }
    }).then(tb_area_di => {
      res.status(200).send(tb_area_di);
    })

  });

  return api;
}
