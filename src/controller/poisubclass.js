import Sequelize from 'sequelize';
import { Router } from 'express';
import Poisubclass from '../model/poisubclass';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    Poisubclass.findAll({
      attributes: ['scname', 'scid','sctag2','clid' ]
    }).then(tb_poisubclass => {
      res.status(200).send(tb_poisubclass );
    })

  });


    return api;
}
