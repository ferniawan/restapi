import Sequelize from 'sequelize';
import { Router } from 'express';
import Poi from '../model/poi';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/:pr/:di/:su/:vi/:scid', authenticate,(req, res) => {

    var params = {};

    if( req.params.pr!='null'){
      params.pr_id = req.params.pr;
    }
    if( req.params.di!='null'){
      params.di_id = req.params.di;
    }
    if( req.params.su!='null'){
      params.su_id = req.params.su;
    }
    if(req.params.vi!='null'){
      params.vi_id = req.params.vi;
    }
    if(req.params.scid !='null'){
      params.scid = req.params.scid;
    }

    Poi.findAll({
      attributes: ['poi_id', 'poi_name', 'poi_latitude' , 'poi_longitude', 'scid'],
      where:params

    }).then(tb_poi => {
      res.status(200).send(tb_poi);
    })

  });


    return api;
}
