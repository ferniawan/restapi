import Sequelize from 'sequelize';
import { Router } from 'express';
import Village from '../model/village';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    Village.findAll({
      attributes: [[db.col('vi_name'), 'area_name'], 'vi_id' , [db.col('vi_geom'), 'geom']]
    }).then(tb_area_vi => {
      res.status(200).send(tb_area_vi);
    })

  });

  // '/v1/village' - GET all village
  api.get('/:id', authenticate,(req, res) => {

    Village.findAll({
      attributes: [[db.col('vi_name'), 'area_name'], 'vi_id' , [db.col('vi_geom'), 'geom']],
      where:{su_id:req.params.id }
    }).then(tb_area_vi => {
      res.status(200).send(tb_area_vi);
    })

  });


  return api;
}
