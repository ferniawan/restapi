import Sequelize from 'sequelize';
import { Router } from 'express';
import Province from '../model/province';
import bodyParser from 'body-parser';
import {authenticate} from '../middleware/authMiddleware';

export default({ config, db }) => {
  let api = Router();

  // '/v1/province' - GET all provinces
  api.get('/', authenticate,(req, res) => {

    Province.findAll({
      attributes: [[db.col('pr_name'), 'area_name'], 'pr_id', [db.col('pr_geom'), 'geom']]
    }).then(tb_area_pr => {
      res.status(200).send( tb_area_pr );
    })

  });
 
    return api;
}
