import Sequelize from 'sequelize';
import config from './config'

export default callback =>{
  let db = new Sequelize(config.pgUrl);
  callback(db);
}
