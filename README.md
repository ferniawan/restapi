REST API EXPRESS v1 27-07-2017
================================

This is a simple REST API Controller, Model, Authenticate paspport, Postgres, Babel, and Eslint

This is the very first version, feel free to use for any app. Contributions are always welcome!


Installation / Running
----------------------

1. npm run build
2. npm run dev to development site
3. npm run start for production useing nodemon pm2
4. Visit the app at [http://localhost:3005](http://localhost:3005)


License
-------
MIT

Cheers,

Ferniawan
