'use strict';

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _passportLocalSequelize = require('passport-local-sequelize');

var _passportLocalSequelize2 = _interopRequireDefault(_passportLocalSequelize);

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _db2.default)(function (db) {

  var Account = db.define('Account', { name: _sequelize2.default.STRING,
    myhash: _sequelize2.default.STRING,
    mysalt: _sequelize2.default.STRING });
  _passportLocalSequelize2.default.attachToUser(Account, {
    usernameField: 'name',
    hashField: 'myhash',
    saltField: 'mysalt'
  });

  module.exports = Account;
});
//# sourceMappingURL=account.js.map