'use strict';

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _db2.default)(function (db) {
  var Task = db.define('tb_area_vi', {
    su_name: _sequelize2.default.STRING
  });
  module.exports = Task;
});
//# sourceMappingURL=village.js.map