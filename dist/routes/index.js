'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _middleware = require('../middleware');

var _middleware2 = _interopRequireDefault(_middleware);

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

var _account = require('../controller/account');

var _account2 = _interopRequireDefault(_account);

var _province = require('../controller/province');

var _province2 = _interopRequireDefault(_province);

var _district = require('../controller/district');

var _district2 = _interopRequireDefault(_district);

var _subdistrict = require('../controller/subdistrict');

var _subdistrict2 = _interopRequireDefault(_subdistrict);

var _village = require('../controller/village');

var _village2 = _interopRequireDefault(_village);

var _poiclass = require('../controller/poiclass');

var _poiclass2 = _interopRequireDefault(_poiclass);

var _poisubclass = require('../controller/poisubclass');

var _poisubclass2 = _interopRequireDefault(_poisubclass);

var _poi = require('../controller/poi');

var _poi2 = _interopRequireDefault(_poi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express2.default)();

//connect to db
(0, _db2.default)(function (db) {

  //internal middleware
  router.use((0, _middleware2.default)({ config: _config2.default, db: db }));

  //api routes v1 (/v1)
  router.use('/account', (0, _account2.default)({ config: _config2.default, db: db }));
  router.use('/province', (0, _province2.default)({ config: _config2.default, db: db }));
  router.use('/district', (0, _district2.default)({ config: _config2.default, db: db }));
  router.use('/subdistrict', (0, _subdistrict2.default)({ config: _config2.default, db: db }));
  router.use('/village', (0, _village2.default)({ config: _config2.default, db: db }));
  router.use('/poi', (0, _poi2.default)({ config: _config2.default, db: db }));
  router.use('/poiclass', (0, _poiclass2.default)({ config: _config2.default, db: db }));
  router.use('/poisubclass', (0, _poisubclass2.default)({ config: _config2.default, db: db }));
});

exports.default = router;
//# sourceMappingURL=index.js.map