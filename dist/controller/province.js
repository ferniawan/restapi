'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _province = require('../model/province');

var _province2 = _interopRequireDefault(_province);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/province' - GET all provinces
  api.get('/', _authMiddleware.authenticate, function (req, res) {

    _province2.default.findAll({
      attributes: [[db.col('pr_name'), 'area_name'], 'pr_id', [db.col('pr_geom'), 'geom']]
    }).then(function (tb_area_pr) {
      res.status(200).send(tb_area_pr);
    });
  });

  return api;
};
//# sourceMappingURL=province.js.map