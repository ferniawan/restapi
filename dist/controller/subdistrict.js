'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _subdistrict = require('../model/subdistrict');

var _subdistrict2 = _interopRequireDefault(_subdistrict);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/province' - GET all provinces
  api.get('/', _authMiddleware.authenticate, function (req, res) {

    _subdistrict2.default.findAll({
      attributes: [[db.col('su_name'), 'area_name'], 'su_id', [db.col('su_geom'), 'geom']]
    }).then(function (tb_area_su) {
      res.status(200).send(tb_area_su);
    });
  });

  // '/v1/village' - GET all village
  api.get('/:id', _authMiddleware.authenticate, function (req, res) {

    _subdistrict2.default.findAll({
      attributes: [[db.col('su_name'), 'area_name'], 'su_id', [db.col('su_geom'), 'geom']],
      where: { di_id: req.params.id }
    }).then(function (tb_area_su) {
      res.status(200).send(tb_area_su);
    });
  });
  return api;
};
//# sourceMappingURL=subdistrict.js.map