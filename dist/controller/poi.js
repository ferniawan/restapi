'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _poi = require('../model/poi');

var _poi2 = _interopRequireDefault(_poi);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/province' - GET all provinces
  api.get('/:pr/:di/:su/:vi/:scid', _authMiddleware.authenticate, function (req, res) {

    var params = {};

    if (req.params.pr != 'null') {
      params.pr_id = req.params.pr;
    }
    if (req.params.di != 'null') {
      params.di_id = req.params.di;
    }
    if (req.params.su != 'null') {
      params.su_id = req.params.su;
    }
    if (req.params.vi != 'null') {
      params.vi_id = req.params.vi;
    }
    if (req.params.scid != 'null') {
      params.scid = req.params.scid;
    }

    _poi2.default.findAll({
      attributes: ['poi_id', 'poi_name', 'poi_latitude', 'poi_longitude', 'scid'],
      where: params

    }).then(function (tb_poi) {
      res.status(200).send(tb_poi);
    });
  });

  return api;
};
//# sourceMappingURL=poi.js.map