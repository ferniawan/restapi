'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _poiclass = require('../model/poiclass');

var _poiclass2 = _interopRequireDefault(_poiclass);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/province' - GET all provinces
  api.get('/', _authMiddleware.authenticate, function (req, res) {

    _poiclass2.default.findAll({
      attributes: ['clname', 'clid']
    }).then(function (tb_poiclass) {
      res.status(200).send(tb_poiclass);
    });
  });

  return api;
};
//# sourceMappingURL=poiclass.js.map