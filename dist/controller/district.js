'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _district = require('../model/district');

var _district2 = _interopRequireDefault(_district);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/district' - GET all provinces
  api.get('/', _authMiddleware.authenticate, function (req, res) {

    _district2.default.findAll({
      attributes: [[db.col('di_name'), 'area_name'], 'di_id', [db.col('di_geom'), 'geom']]
    }).then(function (tb_area_di) {
      res.status(200).send(tb_area_di);
    });
  });

  // '/v1/district' - GET all village
  api.get('/:id', _authMiddleware.authenticate, function (req, res) {

    _district2.default.findAll({
      attributes: [[db.col('di_name'), 'area_name'], 'di_id', [db.col('di_geom'), 'geom']],
      where: { pr_id: req.params.id }
    }).then(function (tb_area_di) {
      res.status(200).send(tb_area_di);
    });
  });

  return api;
};
//# sourceMappingURL=district.js.map