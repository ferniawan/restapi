'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _express = require('express');

var _poisubclass = require('../model/poisubclass');

var _poisubclass2 = _interopRequireDefault(_poisubclass);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // '/v1/province' - GET all provinces
  api.get('/', _authMiddleware.authenticate, function (req, res) {

    _poisubclass2.default.findAll({
      attributes: ['scname', 'scid', 'sctag2', 'clid']
    }).then(function (tb_poisubclass) {
      res.status(200).send(tb_poisubclass);
    });
  });

  return api;
};
//# sourceMappingURL=poisubclass.js.map